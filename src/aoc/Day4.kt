package aoc

fun main(args : Array<String>) {
    val day4 = Day4()
    day4.part1("4-1-test-input.txt")
    day4.part1("4-1-input.txt")
    day4.part2("4-2-test-input.txt")
    day4.part2("4-1-input.txt")
}

class Day4 {
    private val utils = Utils()

    fun part1(path: String) {
        val lines = utils.getStringList(path)
        val count = lines.filter{validate1(it)}.count()
        System.out.println("Part 1: " + count)
    }

    private fun validate1(phrase: String) : Boolean {
        val words = phrase.split(" ")
        return words.size == words.distinct().count()
    }

    fun part2(path: String) {
        val lines = utils.getStringList(path)
        System.out.println("Part 2: " + lines.filter{validate2(it)}.count())
    }

    private fun validate2(phrase: String) : Boolean {
        val words = phrase.split(" ")
        val sorted = words.map { it.toCharArray().sortedWith(compareBy({it})) }
        val distinct = sorted.distinct().count()
        return words.size == distinct
    }
}