package aoc

fun main(args : Array<String>) {
    val day5 = Day5()
    day5.part1("5-1-test-input.txt")
    day5.part1("5-1-input.txt")
    day5.part2("5-1-test-input.txt")
    day5.part2("5-1-input.txt")
}

class Day5 {
    private val utils = Utils()

    fun part1(path: String) {
        val lines = utils.getIntegerArray(path)
        var i = 0
        var steps = 0
        while ( i < lines.size ) {
            val last = i
            i += lines[i]
            lines[last]++
            steps++
        }
        System.out.println("Part 1: " + steps)
    }

    fun part2(path: String) {
        val lines = utils.getIntegerArray(path)
        var i = 0
        var steps = 0
        while ( i < lines.size ) {
            val last = i
            i += lines[i]
            if (lines[last] >= 3)
                lines[last]--
            else
                lines[last]++
            steps++
        }
        System.out.println("Part 2: " + steps)
    }
}