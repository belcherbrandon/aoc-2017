package aoc

fun main(args : Array<String>) {
    val day7test = Day7()
    val root1 = day7test.part1("7-1-test-input.txt")
    System.out.println(root1.name)

    val day7 = Day7()
    val root2 = day7.part1("7-1-input.txt")
    day7.part2(root2)
}

class Day7 {
    private val utils = Utils()
    private val programsMap = HashMap<String, Program>()

    fun part1(input: String): Program {
        val list = utils.getStringList(input)

        list.forEach{
            val lineSplit = it.split(" -> ")
            val programSplit = lineSplit[0].split(" ")
            val name = programSplit[0]
            val weight = programSplit[1].replace(Regex("[()]"), "").toInt()
            var program = programsMap.get(name)
            if (program == null) {
                program = Program(name, weight)
                programsMap.put(name, program)
            } else {
                program.weight = weight
            }

            if (lineSplit.size > 1) {
                lineSplit[1].split(", ").forEach {
                    var child = programsMap.get(it)
                    if (child == null) {
                        child = Program(it)
                        programsMap.put(it, child)
                    }
                    child.parent = program
                    program.children.add(child)
                }
            }

        }
        /*val programsMap = list.map{it.split(" ")}.map{
            it[0] to Program(it[0], it[1].replace(Regex("[()]"), "").toInt())
        }.toMap()*/
        return programsMap.filter{it.value.parent == null}.toList().take(1)[0].second
    }

    fun part2(root : Program) {
        addChildWeights(root)
        System.out.println(programsMap)
    }

    fun addChildWeights(program: Program) : Int {
        var sum = program.weight
        program.children.forEach{
            sum += addChildWeights(it)
        }
        program.sum = sum
        return sum
    }

    data class Program(val name: String, var weight: Int = -1, var parent: Program? = null, var sum: Int = 0, val children: ArrayList<Program> = ArrayList()) {
        override fun toString(): String {
            return name
        }
    }
}