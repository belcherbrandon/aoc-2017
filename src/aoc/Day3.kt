package aoc

fun main(args : Array<String>) {
    val day3 = Day3()
    day3.part1(1024)
    day3.part1(361527)
    day3.part2(361527)
}

class Day3 {
    fun part1(target : Int) {
        var count = 2
        for (iteration in 0 until 400) {
            val sideLength = iteration * 2 + 2
            for (i in 0 until 4) {
                var steps = iteration * 2 + 2
                for (j in 0 until sideLength) {
                    if (j < sideLength / 2) {
                        steps--
                    } else {
                        steps++
                    }

                    //System.out.print("" + steps + " ")
                    if(count == target) {
                        System.out.println()
                        System.out.println("Steps: " + steps)
                        return
                    }
                    count++
                }
            }
        }
    }

    fun part2(target : Int) {
        val size = 20
        val grid = Array(size) { IntArray(size) }
        var x = size / 2
        var y = size / 2
        var d = 'E'
        grid[x][y] = 1

        for (count in 0 until 80) {
            grid[x][y] += grid[x+1][y]
            grid[x][y] += grid[x+1][y+1]
            grid[x][y] += grid[x][y+1]
            grid[x][y] += grid[x-1][y+1]
            grid[x][y] += grid[x-1][y]
            grid[x][y] += grid[x-1][y-1]
            grid[x][y] += grid[x][y-1]
            grid[x][y] += grid[x+1][y-1]

            if (grid[x][y] > target) {
                System.out.println("Part2: " + grid[x][y])
                return
            }

            if (d == 'N') {
                if (grid[x-1][y] == 0) {
                    x--
                    d = 'W'
                }
                else
                    y++
            } else if (d == 'W') {
                if (grid[x][y-1] == 0) {
                    y--
                    d = 'S'
                } else
                    x--
            } else if (d == 'S') {
                if (grid[x+1][y] == 0) {
                    x++
                    d = 'E'
                } else
                    y--
            } else if (d == 'E') {
                if (grid[x][y+1] == 0) {
                    y++
                    d = 'N'
                } else
                    x++
            }
        }
    }
}