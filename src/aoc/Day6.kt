package aoc

fun main(args : Array<String>) {
    val day6 = Day6()
    val input1 = arrayListOf(0,2,7,0)
    val pattern1 = day6.part1(input1)
    day6.part2(input1, pattern1)

    val input2 = arrayListOf(5,1,10,0,1,7,13,14,3,12,8,10,7,12,0,6)
    val pattern2 = day6.part1(input2)
    day6.part2(input2, pattern2)
}

class Day6 {
    fun part1(input: ArrayList<Int>) : String {
        val history = ArrayList<String>(1)
        var cycle = 0
        while(!history.contains(arrayToString(input))) {
            // record history
            history.add(arrayToString(input))

            var largestIndex = 0

            // get largest value for distribution
            for (i in 0 until input.size) {
                if (input[i] > input[largestIndex]) {
                    largestIndex = i
                }
            }

            // distribute
            var distribution = input[largestIndex]
            input[largestIndex] = 0
            while (distribution > 0) {
                input[++largestIndex % input.size]++
                distribution--
            }

            cycle++
        }
        System.out.println("Part 1 cycles: " + cycle)
        return arrayToString(input)
    }

    fun part2(input: ArrayList<Int>, pattern: String) {
        var cycle = 0
        var isCounting = false
        for (i in 0 until 1000000) {
            if (pattern == arrayToString(input)) {
                if (isCounting) {
                    System.out.println("Part 2 cycles: " + cycle)
                    return
                } else {
                    isCounting = true
                }
            }

            var largestIndex = 0

            // get largest value for distribution
            for (j in 0 until input.size) {
                if (input[j] > input[largestIndex]) {
                    largestIndex = j
                }
            }

            // distribute
            var distribution = input[largestIndex]
            input[largestIndex] = 0
            while (distribution > 0) {
                input[++largestIndex % input.size]++
                distribution--
            }

            if (isCounting) {
                cycle++
            }
        }
        System.out.println("Part 1 cycles: " + cycle)
    }

    private fun arrayToString(array: ArrayList<Int>) : String {
        return array.joinToString(",")
    }
}
