package aoc

class Utils {
    fun getIntegerArray(path: String) : IntArray {
        val string = this.javaClass.classLoader.getResource(path).readText()
        return string.split("\n").stream().mapToInt(Integer::parseInt).toArray()
    }

    fun getStringList(path: String) : List<String> {
        val string = this.javaClass.classLoader.getResource(path).readText()
        return string.split("\n")
    }
}