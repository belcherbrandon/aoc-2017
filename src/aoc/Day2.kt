package aoc

fun main(args : Array<String>) {
    val day2 = Day2()
    day2.part1("2-1-test-input.txt")
    day2.part1("2-1-input.txt")
    day2.part2("2-2-test-input.txt")
    day2.part2("2-1-input.txt")
}

class Day2 {
    private val utils = Utils()

    fun part1(path: String) {
        val lines = utils.getStringList(path)
        System.out.println("Part 1 " + lines.map{mapLinePart1(it)}.sum())
    }

    private fun mapLinePart1(line : String) : Int {
        val numbers = line.split("\t").map(Integer::parseInt)

        var min = numbers[0]
        var max = numbers[0]

        numbers.forEach {
            if (it < min) {
                min = it
            } else if (it > max) {
                max = it
            }
        }
        return max - min
    }

    fun part2(path: String) {
        val lines = utils.getStringList(path)
        System.out.println("Part 2 " + lines.map{mapLinePart2(it)}.sum())
    }

    private fun mapLinePart2(line : String) : Int {
        val numbers = line.split("\t").map(Integer::parseInt)
        val length = numbers.size
        for (i in 0 until length) {
            for (j in 0 until length) {
                if (i != j && numbers[i] % numbers[j] == 0) {
                    return numbers[i] / numbers[j]
                }
            }
        }
        return 0
    }
}

